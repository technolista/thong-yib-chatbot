# -*- coding: utf-8 -*-
from flask import Flask, request, jsonify, abort
from pydialogflow_fulfillment import DialogflowRequest
from pydialogflow_fulfillment import DialogflowResponse
from pydialogflow_fulfillment.response import SimpleResponse, OutputContexts

from googleapiclient.discovery import build

import os
import json
import time
from datetime import datetime
from pytz import timezone
from markupsafe import escape
import pandas as pd

SPREADSHEET_ID = os.getenv("THONGYIB_SPREADSHEET_ID")
SPREADSHEET_RANGE = os.getenv("THONGYIB_SPREADSHEET_RANGE")

app = Flask(__name__)

@app.route("/")
def hello_world():
    """Return Hello, World message. Just to check if API is deployable"""
    return "Hello, World"

@app.route("/env/<env_name>")
def get_env(env_name):
    env_value = os.environ.get(env_name)
    response = {
        "name": env_name,
        "value": env_value
    }
    return response

@app.route("/check_intime")
def check_intime():
    THONGYIB_REGISTER_START_TIME = os.getenv("THONGYIB_REGISTER_START_TIME")+" +0700"
    THONGYIB_REGISTER_END_TIME = os.getenv("THONGYIB_REGISTER_END_TIME")+" +0700"

    th_timezone = timezone("Asia/Bangkok")

    start_time = datetime.strptime(THONGYIB_REGISTER_START_TIME,"%Y-%m-%d %H:%M %z").astimezone(th_timezone)
    print(start_time)
    end_time = datetime.strptime(THONGYIB_REGISTER_END_TIME,"%Y-%m-%d %H:%M %z").astimezone(th_timezone)
    print(end_time)

    now_time = datetime.now().astimezone(th_timezone)
    if start_time < now_time < end_time:
        result="intime"
    else:
        if now_time < start_time:
            result="outtime_early"
        elif now_time > end_time:
            result="outtime_late"

    response = {
        "start_time": start_time.strftime("%Y-%m-%d %H:%M"),
        "now_time": now_time.strftime("%Y-%m-%d %H:%M"),
        "end_time": end_time.strftime("%Y-%m-%d %H:%M"),
        "result" : result
    }
    return response

@app.route("/chatbot-webhooks",methods=['POST'])
def chatbot_webhooks():
    """Dialogflow webhook. Return Dialogflow response in JSON format"""
    # dialog_fulfillment = DialogflowRequest(request.data)
    json_data = request.get_json()
    print("=============REQUEST BODY==============")
    print(json_data)
    print("===========END REQUEST BODY============")
    # if(json_data["queryResult"]["action"]=="UserRegister"):
    #     return chatbot_intent_preregister(json_data)  
    if(json_data["queryResult"]["action"]=="UserRegister.UserRegister-Phonenumber"):
        if(chatbot_intent_register_prerequisite(json_data)):
            return chatbot_intent_register(json_data)  
        else:
            return chatbot_intent_outtime()
    else:
        return chatbot_intent_fallback()

def chatbot_intent_register_prerequisite(json_data):
    result = check_intime()
    if(result["result"]=="intime"):
        return True
    else:
        return False

def chatbot_intent_register(json_data):
    """Fulfillment for Dialogflow when intent is UserRegister"""
    context_parameters = {}
    outputContexts = json_data["queryResult"]["outputContexts"]
    for context in outputContexts:
        if "userregister-followup" in context["name"]:
            context_parameters = context["parameters"]
            break
    print("========== CONTEXT PARAMETERS ===============")
    print(context_parameters)
    print("======== END CONTEXT PARAMETERS =============")

    fullname = context_parameters["fullname.original"]
    phonenumber = context_parameters["phonenumber.original"]

    # Connect google sheet API
    service = build('sheets', 'v4')
    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=SPREADSHEET_ID,range=SPREADSHEET_RANGE).execute()
    result_data = result.get('values', [])
    number_of_row = len(result_data)

    # Checking for duplicate
    try:
        userId = json_data["originalDetectIntentRequest"]["payload"]["data"]["source"]["userId"]
    except:
        userId = "TESTER"

    df = pd.DataFrame(result_data[1:],columns=result_data[0])
    df_finding = df[(df["ชื่อ-นามสกุล"]==fullname) & (df["เบอร์โทร"]==phonenumber)]
    if(df_finding.size>0):
        duplicate_response = "คุณได้ลงชื่อเข้าร่วมสนุกแล้ว สามารถติดตามประกาศผู้โชคดีได้ในรายการถ่ายทอดสดวันนี้ค่ะ"
        dialogflow_response = DialogflowResponse(duplicate_response)
        return dialogflow_response.get_final_response()
    else:
        th_timezone = timezone("Asia/Bangkok")
        th_datetime = datetime.now().astimezone(th_timezone)

        row_values = [
            number_of_row,
            fullname,
            phonenumber,
            th_datetime.strftime('%Y-%m-%d %H:%M:%S'),
            userId
        ]
        body = {
            "values": [row_values]
        }
        result = sheet.values().append(spreadsheetId=SPREADSHEET_ID,range=SPREADSHEET_RANGE,valueInputOption="RAW",insertDataOption="INSERT_ROWS",body=body).execute()
        print('{0} cells appended.'.format(result.get('updates').get('updatedCells')))

        success_response = json_data["queryResult"]["fulfillmentText"]
        dialogflow_response = DialogflowResponse(success_response)
        return dialogflow_response.get_final_response()

def chatbot_intent_fallback():
    default_response = """กรุณาติดต่อกลับมาใหม่อีกครั้ง หลังเวลา 22.30 น. ค่ะ"""

    dialogflow_response = DialogflowResponse(default_response)
    return dialogflow_response.get_final_response()

def chatbot_intent_outtime():
    
    result = check_intime()
    if(result["result"]=="outtime_late"):
        response = """ขออภัยค่ะ ขณะนี้ปิดให้ลงทะเบียนแล้ว สามารถติดตามได้ในกิจกรรมถัดไปค่ะ"""
    elif(result["result"]=="outtime_early"):
        response = """ขออภัยค่ะ ขณะนี้ยังไม่เปิดให้ลงทะเบียน สามารถลงทะเบียนตั้งแต่เวลา 19.00 น. เป็นต้นไปค่ะ"""
    else:
        response = """กรุณาติดต่อกลับมาใหม่อีกครั้ง หลังเวลา 22.30 น. ค่ะ"""
    dialogflow_response = DialogflowResponse(response)
    return dialogflow_response.get_final_response()
