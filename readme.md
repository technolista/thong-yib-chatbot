Project name : rajadamnern-stadium-thong-yib
Dialog Flow agent name : rjdnsd-thong-yib

# Setup

## Prerequisite

- Python 3.8
- Pip
- Virtualenv

## Create Virtualenv
```
virtualenv .env -p python3.8
```

## Activate virtual environment

### On Linux
```
source ./.env/bin/activate
```

## Install required packages
```
pip install -r requirements.txt
```

## Debug mode

```
export FLASK_ENV=development
flask run
```

## Google Service Account

```
export GOOGLE_APPLICATION_CREDENTIALS=rjdnsd-thong-yib-iasv-6966c1867fb4.json
```

## For VSCode
### settings.json

```
{
    // Use IntelliSense to learn about possible attributes.
    // Hover to view descriptions of existing attributes.
    // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Python: Flask",
            "type": "python",
            "request": "launch",
            "module": "flask",
            "env": {
                "FLASK_APP": "app.py",
                "FLASK_ENV": "development",
                "FLASK_DEBUG": "0"
            },
            "args": [
                "run"
            ],
        }
    ]
}
```

### launch.json

```
{
    "python.pythonPath": ".env/bin/python",
    "python.linting.enabled": true,
    "python.testing.pytestArgs": [
        "."
    ],
    "python.testing.unittestEnabled": false,
    "python.testing.nosetestsEnabled": false,
    "python.testing.pytestEnabled": true
}
```


## Dialogflow - Sample Log

Sample Interaction Log

```
{
  "queryText": "0825206777",
  "action": "UserRegister.UserRegister-Phonenumber",
  "parameters": {},
  "fulfillmentText": "ขอบคุณที่ร่วมกิจกรรมกับเราค่ะ\nสามารถติดตามกฎกติกาและการประกาศผู้โชคดีได้\nในรายการถ่ายทอดสดของวันนี้ได้เลยค่ะ",
  "fulfillmentMessages": [
    {
      "text": {
        "text": [
          "ขอบคุณที่ร่วมกิจกรรมกับเราค่ะ\nสามารถติดตามกฎกติกาและการประกาศผู้โชคดีได้\nในรายการถ่ายทอดสดของวันนี้ได้เลยค่ะ"
        ]
      },
      "lang": "th"
    }
  ],
  "outputContexts": [
    {
      "name": "userregister-name-followup",
      "lifespanCount": 1,
      "parameters": {
        "fullname": "ภาคภูมิ วิเศษณัฐ",
        "fullname.original": "ภาคภูมิ วิเศษณัฐ"
      }
    }
  ],
  "intent": {
    "id": "6fa96ead-4c45-43d1-8552-097bf1e69ca1",
    "displayName": "User Register - Phone number",
    "priority": 500000,
    "inputContextNames": [
      "UserRegister-Name-followup"
    ],
    "action": "UserRegister.UserRegister-Phonenumber",
    "parameters": [
      {
        "id": "e559c753-028c-4eea-b01d-fd96487b77d6",
        "displayName": "phonenumber",
        "value": "$phonenumber",
        "entityTypeDisplayName": "@phonenumber_th"
      }
    ],
    "messages": [
      {
        "text": {
          "text": [
            "ขอบคุณที่ร่วมกิจกรรมกับเราค่ะ\nสามารถติดตามกฎกติกาและการประกาศผู้โชคดีได้\nในรายการถ่ายทอดสดของวันนี้ได้เลยค่ะ"
          ]
        },
        "lang": "th"
      }
    ],
    "endInteraction": true,
    "rootFollowupIntentId": "08848321-3403-446e-ab0d-645839024b9c",
    "parentFollowupIntentId": "4f9a5cb8-7cb5-4662-8bf0-c0ebadd764b9"
  },
  "intentDetectionConfidence": 0.7429059,
  "diagnosticInfo": {
    "end_conversation": true
  },
  "languageCode": "th",
  "slotfillingMetadata": {
    "allRequiredParamsPresent": true
  },
  "id": "5defe3ee-d128-4a59-8157-dee8073bfa6d-0820055c",
  "sessionId": "1c4320ea-16f6-5364-2926-876f2029521b",
  "timestamp": "2020-08-05T06:12:36.894Z",
  "source": "agent",
  "agentEnvironmentId": {
    "agentId": "f0d03ab8-dea5-482b-ba33-c6fa0a181e36",
    "cloudProjectId": "rjdnsd-thong-yib-iasv"
  }
}

```

Sample output context

```
{
"name": "projects/rjdnsd-thong-yib-iasv/agent/sessions/1c4320ea-16f6-5364-2926-876f2029521b/contexts/userregisterexplicit-given-name-last-name-followup",
"lifespanCount": 4,
"parameters": {
    "number.original": "02856314569",
    "number": 2856314569,
    "phone-number": 984563217,
    "given-name.original": "ธนาบัตร",
    "last-name.original": "ศิริวัฒธนา",
    "given-name": "ธนาบัตร",
    "phone-number.original": "0984563217",
    "last-name": "ศิริวัฒธนา"
}
}
```
