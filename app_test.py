# -*- coding: utf-8 -*-
import os
import pytest
import json
import re
from pytz import timezone
from datetime import datetime,timedelta
from app import app
from faker import Faker

th_timezone = timezone("Asia/Bangkok")

@pytest.fixture
def client():
    app.config["TESTING"] = True
    yield app.test_client()

def test_helloworld(client):
    response = client.get("/")
    assert b'Hello, World' in response.data

def test_envvar_THONGYIB_SPREADSHEET_ID(client):
    os.environ["THONGYIB_SPREADSHEET_ID"]="1jI1TtdJEERqXmg_m6o4vPRY4H06_71wbRlBHZ2HrV08"
    response = client.get("/env/THONGYIB_SPREADSHEET_ID")
    print(response.data)
    response_json = json.loads(response.data)
    assert '1jI1TtdJEERqXmg_m6o4vPRY4H06_71wbRlBHZ2HrV08' == response_json["value"]

def test_envvar_THONGYIB_SPREADSHEET_RANGE(client):
    os.environ["THONGYIB_SPREADSHEET_RANGE"]="Data!A:E"
    response = client.get("/env/THONGYIB_SPREADSHEET_RANGE")
    print(response.data)
    response_json = json.loads(response.data)
    assert 'Data!A:E' == response_json["value"]

def test_envvar_THONGYIB_REGISTER_START_TIME(client):
    os.environ["THONGYIB_REGISTER_START_TIME"]="2020-08-06 07:00"
    response = client.get("/env/THONGYIB_REGISTER_START_TIME")
    # strptime
    print(response.data)
    response_json = json.loads(response.data)
    assert '2020-08-06 07:00' == response_json["value"]

def test_envvar_THONGYIB_REGISTER_END_TIME(client):
    os.environ["THONGYIB_REGISTER_END_TIME"]="2020-08-06 12:00"
    response = client.get("/env/THONGYIB_REGISTER_END_TIME")
    # strptime
    print(response.data)
    response_json = json.loads(response.data)
    assert '2020-08-06 12:00' == response_json["value"]

def test_envvar_GOOGLE_APPLICATION_CREDENTIALS(client):
    os.environ["GOOGLE_APPLICATION_CREDENTIALS"]="rjdnsd-thong-yib-iasv-6966c1867fb4.json"
    response = client.get("/env/GOOGLE_APPLICATION_CREDENTIALS")
    print(response.data)
    response_json = json.loads(response.data)
    assert 'rjdnsd-thong-yib-iasv-6966c1867fb4.json' == response_json["value"]

def test_check_outtime_early(client):
    start_time = datetime.now().astimezone(th_timezone) + timedelta(hours=1)
    end_time = datetime.now().astimezone(th_timezone) + timedelta(hours=2)

    os.environ["THONGYIB_REGISTER_START_TIME"]=start_time.strftime("%Y-%m-%d %H:%M")
    os.environ["THONGYIB_REGISTER_END_TIME"]=end_time.strftime("%Y-%m-%d %H:%M")

    response = client.get("/check_intime")
    assert b'outtime' in response.data

def test_check_outtime_late(client):
    start_time = datetime.now().astimezone(th_timezone) - timedelta(hours=2)
    end_time = datetime.now().astimezone(th_timezone) - timedelta(hours=1)

    os.environ["THONGYIB_REGISTER_START_TIME"]=start_time.strftime("%Y-%m-%d %H:%M")
    os.environ["THONGYIB_REGISTER_END_TIME"]=end_time.strftime("%Y-%m-%d %H:%M")

    response = client.get("/check_intime")
    assert b'outtime' in response.data

def test_check_intime(client):
    start_time = datetime.now().astimezone(th_timezone) - timedelta(hours=1)
    end_time = datetime.now().astimezone(th_timezone) + timedelta(hours=1)

    os.environ["THONGYIB_REGISTER_START_TIME"]=start_time.strftime("%Y-%m-%d %H:%M")
    os.environ["THONGYIB_REGISTER_END_TIME"]=end_time.strftime("%Y-%m-%d %H:%M")

    response = client.get("/check_intime")
    assert b'intime' in response.data

def set_testenv():
    os.environ["THONGYIB_SPREADSHEET_ID"]="1jI1TtdJEERqXmg_m6o4vPRY4H06_71wbRlBHZ2HrV08"
    os.environ["THONGYIB_SPREADSHEET_RANGE"]="Data!A:E"
    os.environ["GOOGLE_APPLICATION_CREDENTIALS"]="rjdnsd-thong-yib-iasv-6966c1867fb4.json"

def test_webhook(client):
    set_testenv()
    with open("__test__/sample1.json") as json_file:
        request_data = json.load(json_file)
        assert 'UserRegister.UserRegister-Phonenumber' == request_data['queryResult']['action']

        fake = Faker(['th_TH'])
        # To generate fake name and phonenumber
        outputContexts = request_data["queryResult"]["outputContexts"]
        for index,context in enumerate(outputContexts):
            if "userregister-followup" in context["name"]:
                request_data["queryResult"]["outputContexts"][index]["parameters"]["fullname"]= fake.name()
                request_data["queryResult"]["outputContexts"][index]["parameters"]["fullname.original"]= fake.name()
                request_data["queryResult"]["outputContexts"][index]["parameters"]["phonenumber"]= fake.phone_number()
                request_data["queryResult"]["outputContexts"][index]["parameters"]["phonenumber.original"]= fake.phone_number()

        response = client.post("/chatbot-webhooks",json=request_data)
        response_json = json.loads(response.data)
        assert 'ขอบคุณที่ร่วมกิจกรรมกับเราค่ะ\nสามารถติดตามกฎกติกาและการประกาศผู้โชคดีได้\nในรายการถ่ายทอดสดของวันนี้ได้เลยค่ะ'==response_json["fulfillmentText"]

def test_webhook_duplicated(client):
    set_testenv()
    with open("__test__/sample1.json") as json_file:
        request_data = json.load(json_file)
        assert 'UserRegister.UserRegister-Phonenumber' == request_data['queryResult']['action']

        fake = Faker(['th_TH'])
        # To generate fake name and phonenumber
        outputContexts = request_data["queryResult"]["outputContexts"]
        for index,context in enumerate(outputContexts):
            if "userregister-followup" in context["name"]:
                request_data["queryResult"]["outputContexts"][index]["parameters"]["fullname"]= fake.name()
                request_data["queryResult"]["outputContexts"][index]["parameters"]["fullname.original"]= fake.name()
                request_data["queryResult"]["outputContexts"][index]["parameters"]["phonenumber"]= fake.phone_number()
                request_data["queryResult"]["outputContexts"][index]["parameters"]["phonenumber.original"]= fake.phone_number()

        response = client.post("/chatbot-webhooks",json=request_data)
        response_json = json.loads(response.data)
        assert 'ขอบคุณที่ร่วมกิจกรรมกับเราค่ะ\nสามารถติดตามกฎกติกาและการประกาศผู้โชคดีได้\nในรายการถ่ายทอดสดของวันนี้ได้เลยค่ะ'==response_json["fulfillmentText"]

        response = client.post("/chatbot-webhooks",json=request_data)
        response_json = json.loads(response.data)
        assert 'คุณได้ลงชื่อเข้าร่วมสนุกแล้ว สามารถติดตามประกาศผู้โชคดีได้ในรายการถ่ายทอดสดวันนี้ค่ะ'==response_json["fulfillmentText"]

def test_webhook_outtime_early(client):
    set_testenv()
    start_time = datetime.now().astimezone(th_timezone) + timedelta(hours=1)
    end_time = datetime.now().astimezone(th_timezone) + timedelta(hours=2)
    os.environ["THONGYIB_REGISTER_START_TIME"]=start_time.strftime("%Y-%m-%d %H:%M")
    os.environ["THONGYIB_REGISTER_END_TIME"]=end_time.strftime("%Y-%m-%d %H:%M")
    with open("__test__/sample1.json") as json_file:
        request_data = json.load(json_file)
        assert 'UserRegister.UserRegister-Phonenumber' == request_data['queryResult']['action']

        fake = Faker(['th_TH'])
        # To generate fake name and phonenumber
        outputContexts = request_data["queryResult"]["outputContexts"]
        for index,context in enumerate(outputContexts):
            if "userregister-followup" in context["name"]:
                request_data["queryResult"]["outputContexts"][index]["parameters"]["fullname"]= fake.name()
                request_data["queryResult"]["outputContexts"][index]["parameters"]["fullname.original"]= fake.name()
                request_data["queryResult"]["outputContexts"][index]["parameters"]["phonenumber"]= fake.phone_number()
                request_data["queryResult"]["outputContexts"][index]["parameters"]["phonenumber.original"]= fake.phone_number()

        response = client.post("/chatbot-webhooks",json=request_data)
        response_json = json.loads(response.data)
        assert 'ขออภัยค่ะ ขณะนี้ยังไม่เปิดให้ลงทะเบียน สามารถลงทะเบียนตั้งแต่เวลา 19.00 น. เป็นต้นไปค่ะ'==response_json["fulfillmentText"]

def test_webhook_outtime_late(client):
    set_testenv()
    start_time = datetime.now().astimezone(th_timezone) - timedelta(hours=2)
    end_time = datetime.now().astimezone(th_timezone) - timedelta(hours=1)
    os.environ["THONGYIB_REGISTER_START_TIME"]=start_time.strftime("%Y-%m-%d %H:%M")
    os.environ["THONGYIB_REGISTER_END_TIME"]=end_time.strftime("%Y-%m-%d %H:%M")
    with open("__test__/sample1.json") as json_file:
        request_data = json.load(json_file)
        assert 'UserRegister.UserRegister-Phonenumber' == request_data['queryResult']['action']

        fake = Faker(['th_TH'])
        # To generate fake name and phonenumber
        outputContexts = request_data["queryResult"]["outputContexts"]
        for index,context in enumerate(outputContexts):
            if "userregister-followup" in context["name"]:
                request_data["queryResult"]["outputContexts"][index]["parameters"]["fullname"]= fake.name()
                request_data["queryResult"]["outputContexts"][index]["parameters"]["fullname.original"]= fake.name()
                request_data["queryResult"]["outputContexts"][index]["parameters"]["phonenumber"]= fake.phone_number()
                request_data["queryResult"]["outputContexts"][index]["parameters"]["phonenumber.original"]= fake.phone_number()

        response = client.post("/chatbot-webhooks",json=request_data)
        response_json = json.loads(response.data)
        assert 'ขออภัยค่ะ ขณะนี้ปิดให้ลงทะเบียนแล้ว สามารถติดตามได้ในกิจกรรมถัดไปค่ะ'==response_json["fulfillmentText"]
